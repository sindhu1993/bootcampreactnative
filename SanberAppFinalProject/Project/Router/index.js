import React from 'react';
import { StyleSheet, Text, View } from 'react-native'
import {createStackNavigator} from '@react-navigation/stack'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import {NavigationContainer} from '@react-navigation/native'
import {createDrawerNavigator} from '@react-navigation/drawer'
import LoginScreen from '../../Project/Pages/LoginScreen'
import HomeScreen from '../../Project/Pages/HomeScreen'
import ProfileScreen from '../../Project/Pages/ProfileScreen'
import DetailScreen from '../../Project/Pages/DetailScreen'
import ProcedureRecipe from '../../Project/Pages/ProcedureRecipe'
import SplashScreen from '../../Project/Pages/SplashScreen'
import RegisterScreen from '../Pages/RegisterScreen'
import Icon from 'react-native-vector-icons/Feather'

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="SplashScreen" component={SplashScreen} options={{  headerShown:false }}/>
                <Stack.Screen name="LoginScreen" component={LoginScreen} options={{ title:'Login',headerShown: false }} />
                {/* <Stack.Screen name="HomeScreen" component={HomeScreen} options={{ title:'Home',}}/> */}
                <Stack.Screen name="MainApp" component={MainApp} options={{headerShown: false }} />
                {/* <Stack.Screen name="MyDrawer" component={MyDrawer} /> */}
                <Stack.Screen name="DetailScreen" component={DetailScreen} options={{ title:'List Categories Meal' }}/>
                <Stack.Screen name="ProcedureRecipe" component={ProcedureRecipe} options={{ title:'Recipe Meal' }}/>
                <Stack.Screen name="RegisterScreen" component={RegisterScreen} options={{ title:'Register Account' }}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const MainApp=()=>{
    return (
    <Tab.Navigator>
        <Tab.Screen name="HomeScreen" component={HomeScreen} 
            options={{ 
                tabBarLabel:'Home',
                tabBarIcon:({color,size}) => <Icon name="home" size={size} color={color}/>
            }}/>
        <Tab.Screen name="ProfileScreen" component={ProfileScreen}
            options={{ 
                tabBarLabel:'Profile',
                tabBarIcon:({color,size})=> <Icon name="user" size={size} color={color} />    
            }}
        />
    </Tab.Navigator>)
}

const styles = StyleSheet.create({})
