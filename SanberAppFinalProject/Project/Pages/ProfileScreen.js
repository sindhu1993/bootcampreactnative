import React,{useState,useEffect} from 'react'
import { StyleSheet, Text, Image,View,Button } from 'react-native'
import firebase from 'firebase'

export default function ProfileScreen({navigation}) {

    const [user, setUser] = useState({})
    let user_email = ""

    useEffect(()=>{
        const userInfo = firebase.auth().currentUser
        // console.log(userInfo.providerData.length)
        setUser(userInfo)
        if(userInfo.providerData.length > 0)
        {
            setUser(userInfo)
            user_email += user.email
        }else{
            setUser(userInfo)
        }
        
    },[])


    const logout=()=>{

        firebase.auth().signOut()
        .then(()=>{
            // setUser([])
            navigation.navigate("LoginScreen")
        }).catch(()=>{
            console.log('Login gagal')
        })
    }

    return (
        <View style={styles.container}>
            <Image style={{height:300, width:150}} source={require('../../assets/logo.png')} />
            <Text style={{ fontSize:18,fontWeight:'bold',marginTop:-50 }}>Profile Cheef</Text>
            <View style={{ marginTop:10 }}>
                <Text>Email : {user.email}</Text>
                <Text  style={{ marginBottom:10 }}>Phone Number : 787878</Text>
                <Button onPress={logout} color="#F05ACF" title="Logout"/>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        marginTop:-100,
        backgroundColor:'white',
        justifyContent:'center',
        alignItems:'center'
    }
})
