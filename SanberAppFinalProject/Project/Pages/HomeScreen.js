import React,{useState,useEffect} from 'react'
import { StyleSheet, Text, View, Button, TouchableHighlight,TouchableOpacity, Image } from 'react-native'
import firebase from 'firebase'
import { Data }from './data'
import { FlatList } from 'react-native-gesture-handler'
import Axios from 'react-native-axios'

export default function HomeScreen({navigation}) {

    const firebaseConfig = {
        apiKey: "AIzaSyAYovVBfe6rDI1Roo3xfIIyphloP3uLQ0A",
        authDomain: "authenticationfirebasern-99339.firebaseapp.com",
        projectId: "authenticationfirebasern-99339",
        storageBucket: "authenticationfirebasern-99339.appspot.com",
        messagingSenderId: "1021288969240",
        appId: "1:1021288969240:web:919352e5934d487348bf6d"
    };

    if(!firebase.apps.length){
        firebase.initializeApp(firebaseConfig);
    }

    const [recipe, setRecipe] = useState([])


    const getData=()=>{
        Axios.get(`https://www.themealdb.com/api/json/v1/1/categories.php`)
        .then(res => {
            // console.log("res : ",res.data.categories)
            setRecipe(res.data.categories)
        })
    }

    const [user, setUser] = useState({})

    useEffect(() => {
        getData();
        const userInfo = firebase.auth().currentUser
        console.log(userInfo)
        setUser(userInfo)
    }, [])

    const detailRecipe=(strCategory)=>{
        navigation.navigate("DetailScreen",{strCategory:strCategory})
        console.log(strCategory)
    }


    return (
        <View style={styles.container}>
            <View style={{flexDirection:'row', justifyContent:"space-between", padding: 16,
            marginTop:30}}>
                <View>
                    <Text>Welcome Back,</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}>Hello, Chef {user.email}</Text>
                    <Text style={{fontSize:12, fontWeight:'bold'}}>Make Your Own Food, </Text>
                    <Text style={{fontWeight:'bold'}}>Stay at <Text style={{color:'#F0A514'}}>Home</Text></Text>
                </View>
            </View>
            <View style={{alignItems:'center',  marginBottom: 20, paddingBottom: 60}}>

                <View style={{ marginBottom:80 }}>
                    <FlatList 
                        data={recipe}
                        numColumns={2}
                        keyExtractor={item=> item.idCategory }
                        showsVerticalScrollIndicator={false}
                        renderItem={({item})=>{
                            return(
                                <View style={styles.content}>
                                    <TouchableOpacity onPress={()=>detailRecipe(item.strCategory)}>
                                    <Image style={{width: 120, height: 100, alignSelf: 'center', margin:10}} source={{uri:item.strCategoryThumb}}/>
                                    </TouchableOpacity>
                                    <Text style={{fontSize:14,fontWeight:'bold',color:'#F05ACF'}}>{item.strCategory} </Text>
                                </View>
                            )
                        }}
                    />
                </View>
            </View>
        </View>

    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,        
        backgroundColor:'white',
    },  
    content:{
        width: 150,
        height: 150,        
        margin: 5,
        borderWidth:1,
        alignItems:'center',
        borderRadius: 5,
        borderColor:'grey',
    },
})
