import React,{useState,useEffect} from 'react'
import { StyleSheet, Text, View, Button, TouchableHighlight,TouchableOpacity, Image } from 'react-native'
import firebase from 'firebase'
import { FlatList } from 'react-native-gesture-handler'
import Axios from 'react-native-axios'

export default function DetailScreen({route,navigation}) {

    const paramsCategory = route.params

    // https://www.themealdb.com/api/json/v1/1/filter.php?c=Beef

    const [recipe, setRecipe] = useState([])


    const getData=()=>{
        // console.log(`https://www.themealdb.com/api/json/v1/1/filter.php?c=${paramsCategory}`)
        Axios.get(`https://www.themealdb.com/api/json/v1/1/filter.php?c=${paramsCategory.strCategory}`)
        .then(res => {
            // console.log("res : ",res.data.categories)
            setRecipe(res.data.meals)
        })
    }

    useEffect(() => {
        getData();
    }, [])

    const procedureRecipe=(idMeal)=>{
        navigation.navigate("ProcedureRecipe",{idMeal:idMeal})
        // console.log(strCategory)
    }

    return (
        <View style={styles.container}>
            <View style={{flexDirection:'row', justifyContent:"space-between", padding: 16}}>
                <View>
                    <Text style={{fontSize:18, fontWeight:'bold'}}>{paramsCategory.strCategory} Categories</Text>
                </View>
            </View>
            <View style={{alignItems:'center',  marginBottom: 20, paddingBottom: 60}}>
           <FlatList 
                data={recipe}
                numColumns={1}
                keyExtractor={item=> item.idMeal }
                showsVerticalScrollIndicator={false}
                renderItem={({item})=>{
                    return(
                        <View style={{ padding:5,width:'95%'}}>
                            <View style={styles.itemContainer}>
                                <TouchableOpacity onPress={()=>procedureRecipe(item.idMeal)}>
                                <Image style={styles.avatar} source={{uri:item.strMealThumb}}/>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={()=>procedureRecipe(item.idMeal)}>
                                <View style={styles.desc}>
                                    <Text style={styles.descTitle}>{item.strMeal}</Text>
                                </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    )
                }}
            />
              </View>
        </View>

    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,        
        backgroundColor:'white', 
    },  
    content:{
        maxWidth:100,
        width: 150,
        height: 150,        
        margin: 5,
        borderWidth:1,
        alignItems:'center',
        borderRadius: 5,
        borderColor:'grey',
    },
    avatar:{width:80, height:80, borderRadius:100},
    itemContainer:{padding:5,flexDirection:'row',width:'100%',backgroundColor:'#FFE1E1',borderRadius:10},
    desc:{marginLeft:18,marginTop:20,flex:1,width:'90%'},
    descTitle:{fontSize:16,marginBottom:10,maxWidth: '90%'},
    descValue:{fontSize:12},
})
