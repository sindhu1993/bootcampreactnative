import React,{useState,useEffect} from 'react'
import { StyleSheet, Text, View, FlatList, TouchableOpacity, Image,Linking } from 'react-native'
import Axios from 'react-native-axios'

export default function ProcedureRecipe({route,navigation}) {

    const paramsId = route.params

    const [recipe, setRecipe] = useState([])

    let string_ingredients_measure = ""
    let string_instruction = ""
    let str_title = ""
    let str_sub_title = "" 
    let str_youtube = ""
    let str_source = ""

    // strInstructions

    const getData=()=>{
        // console.log(`https://www.themealdb.com/api/json/v1/1/lookup.php?i=53016`)
        Axios.get(`https://www.themealdb.com/api/json/v1/1/lookup.php?i=${paramsId.idMeal}`)
        .then(res => {
            // console.log("res : ",res.data.meals)
            setRecipe(res.data.meals)
            // console.log(arrRecipe)
        })
    }

    for(let x =0; x<recipe.length; x++)
        {
            str_title += `${recipe[x].strMeal}`;
            str_sub_title += `${recipe[x].strCategory} | ${recipe[x].strArea}`;
            str_youtube += `${recipe[x].strYoutube}`;
            str_source += `${recipe[x].strSource}`;
            // ingredients
            if(recipe[x].strIngredient1 !== "" && recipe[x].strMeasure1 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient1+" \t : "+recipe[x].strMeasure1;
            }
            if(recipe[x].strIngredient2 !== "" && recipe[x].strMeasure2 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient2+" \t : "+recipe[x].strMeasure2;
            }
            if(recipe[x].strIngredient3 !== "" && recipe[x].strMeasure3 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient3+" \t : "+recipe[x].strMeasure3;
            }
            if(recipe[x].strIngredient4 !== "" && recipe[x].strMeasure4 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient4+" \t : "+recipe[x].strMeasure4;
            }
            if(recipe[x].strIngredient5 !== "" && recipe[x].strMeasure5 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient5+" \t : "+recipe[x].strMeasure5;
            }

            if(recipe[x].strIngredient6 !== "" && recipe[x].strMeasure6 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient6+" \t : "+recipe[x].strMeasure6;
            }
            if(recipe[x].strIngredient7 !== "" && recipe[x].strMeasure7 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient7+" \t : "+recipe[x].strMeasure7;
            }
            if(recipe[x].strIngredient8 !== "" && recipe[x].strMeasure8 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient8+" \t : "+recipe[x].strMeasure8;
            }
            if(recipe[x].strIngredient9 !== "" && recipe[x].strMeasure9 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient9+" \t : "+recipe[x].strMeasure9;
            }
            if(recipe[x].strIngredient10 !== "" && recipe[x].strMeasure10 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient10+" \t : "+recipe[x].strMeasure10;
            }

            if(recipe[x].strIngredient11 !== "" && recipe[x].strMeasure11 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient11+" \t : "+recipe[x].strMeasure11;
            }
            if(recipe[x].strIngredient12 !== "" && recipe[x].strMeasure12 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient12+" \t : "+recipe[x].strMeasure12;
            }
            if(recipe[x].strIngredient13 !== "" && recipe[x].strMeasure13 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient13+" \t : "+recipe[x].strMeasure13;
            }
            if(recipe[x].strIngredient14 !== "" && recipe[x].strMeasure14 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient14+" \t : "+recipe[x].strMeasure14;
            }
            if(recipe[x].strIngredient15 !== "" && recipe[x].strMeasure15 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient15+" \t : "+recipe[x].strMeasure15;
            }

            if(recipe[x].strIngredient16 !== "" && recipe[x].strMeasure16 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient16+" \t : "+recipe[x].strMeasure16;
            }
            if(recipe[x].strIngredient17 !== "" && recipe[x].strMeasure17 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient17+" \t : "+recipe[x].strMeasure17;
            }
            if(recipe[x].strIngredient18 !== "" && recipe[x].strMeasure18 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient18+" \t : "+recipe[x].strMeasure18;
            }
            if(recipe[x].strIngredient19 !== "" && recipe[x].strMeasure19 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient19+" \t : "+recipe[x].strMeasure19;
            }
            if(recipe[x].strIngredient20 !== "" && recipe[x].strMeasure20 !== " "){
                string_ingredients_measure += '\n \u2022 '+recipe[x].strIngredient20+" \t : "+recipe[x].strMeasure20;
            }
            // ingredients

            // instruction
            if(recipe[x].strInstructions !== " "){
                string_instruction += recipe[x].strInstructions;
            }
            // instruction
        }

    useEffect(() => {
        getData();
    }, [])


    return (
        <View style={styles.container}>
            {/* <View style={{ padding:12 }}> */}
            <Text style={{fontSize:18, fontWeight:'bold'}}>{str_title}</Text>
            <Text style={{fontSize:14}}>{str_sub_title}</Text>
            <FlatList 
                data={recipe}
                numColumns={1}
                keyExtractor={item=> item.idMeal }
                showsVerticalScrollIndicator={false}
                renderItem={({item})=>{
                    return(
                        <View style={{borderRadius:10,flexWrap:'wrap',flexDirection:'row'}}>
                            <Image style={{width:'100%',height:300,borderRadius:10,
                             margin:10}} source={{uri:item.strMealThumb}}/>
                        </View>
                    )
                }}
            />
            

            <Text style={{fontSize:14,fontWeight:'bold'}}>Ingredients :</Text>
            <FlatList 
                data={recipe}
                numColumns={1}
                keyExtractor={item=> item.idMeal }
                showsVerticalScrollIndicator={false}
                renderItem={({item})=>{
                    return(
                        <View style={{ borderRadius:10,padding:12, backgroundColor:'#FBEAEA' }}>
                            <Text>{string_ingredients_measure}</Text>
                        </View>
                    )
                }}
            />

            <Text style={{fontSize:14,fontWeight:'bold',marginTop:10}}>Instructions :</Text>
            <FlatList 
                data={recipe}
                numColumns={1}
                keyExtractor={item=> item.idMeal }
                showsVerticalScrollIndicator={false}
                renderItem={({item})=>{
                    return(
                        <View style={{ borderRadius:10,padding:12,backgroundColor:'#F1ECEC' }}>
                            <Text>{string_instruction}</Text>
                        </View>
                    )
                }}
            />

            <Text style={{fontSize:14,fontWeight:'bold',marginTop:10}}>Further Information :</Text>
            <View style={{ borderRadius:10,padding:12,backgroundColor:'#d0ffdb' }}>
                <Text onPress={() => Linking.openURL(`${str_youtube}`)}>Check On Youtube</Text>
                <Text onPress={() => Linking.openURL(`${str_source}`)}>Source</Text>
            </View>

            {/* </View> */}
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        padding:12,        
        backgroundColor:'white', 
    },  
    content:{
        maxWidth:100,
        width: 150,
        height: 150,        
        margin: 5,
        borderWidth:1,
        alignItems:'center',
        borderRadius: 5,
        borderColor:'grey',
    },
})
