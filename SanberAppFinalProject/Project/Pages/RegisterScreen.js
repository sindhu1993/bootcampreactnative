import React,{useState} from 'react'
import { StyleSheet, Text, View,Image,Button,TextInput } from 'react-native'
import firebase from 'firebase'

export default function RegisterScreen({navigation}) {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const firebaseConfig = {
        apiKey: "AIzaSyAYovVBfe6rDI1Roo3xfIIyphloP3uLQ0A",
        authDomain: "authenticationfirebasern-99339.firebaseapp.com",
        projectId: "authenticationfirebasern-99339",
        storageBucket: "authenticationfirebasern-99339.appspot.com",
        messagingSenderId: "1021288969240",
        appId: "1:1021288969240:web:919352e5934d487348bf6d"
    };

    if(!firebase.apps.length){
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
    }

    const register=()=>{
        // console.log(email,password);
        const data = {
            email,
            password
        }
        console.log(data)
        firebase.auth().createUserWithEmailAndPassword(email,password)
        .then(()=>{
            console.log('Register berhasil')
            navigation.navigate("LoginScreen")
        }).catch(()=>{
            console.log('Register gagal')
        })
    }


    return (
        <View style={styles.container}>
            <Image style={{height:300, width:150}} source={require('../../assets/logo.png')} />
            <View>
                <TextInput 
                    style={{borderWidth: 1, paddingVertical: 10,borderRadius: 5, width: 300,marginBottom: 10, 
                    borderColor:'#A285DF',
                    paddingHorizontal: 10}} 
                    placeholder="Masukan email"
                    value={email}
                    onChangeText={(value)=>setEmail(value)}
                />
                <TextInput 
                    style={{borderWidth: 1, 
                    borderColor:'#A285DF',
                    paddingVertical: 10,borderRadius: 5, width: 300,marginBottom: 10, paddingHorizontal: 10}} 
                    placeholder="Masukan Password"
                    value={password}
                    onChangeText={(value)=>setPassword(value)}
                />
                <Button onPress={register} title="Daftar"/>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        marginTop:-100,
        backgroundColor:'white',
        justifyContent:'center',
        alignItems:'center'
    }
})
