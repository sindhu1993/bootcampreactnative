import React,{useEffect} from 'react'
import { StyleSheet, Image,View } from 'react-native'

export default function SplashScreen({navigation}) {

    useEffect(()=>{
        setTimeout(()=>{
            navigation.navigate("LoginScreen")
        }, 3000)
    },[])

    return (
        <View  style={styles.container}>
            <Image style={{height:300, width:300,resizeMode:'cover'}} source={require('../../assets/logo.png')} />
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white',
        justifyContent:'center',
        alignItems:'center'
    }
})
