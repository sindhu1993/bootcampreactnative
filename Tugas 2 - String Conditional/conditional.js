// If-else
var nama = "John";
var peran = "";

if(nama == "" || peran == "")
{
    console.log('Nama harus Diisi');
}
else
{
    if(nama == "John" && peran == "")
    {
        console.log('Halo '+nama+", Pilih peranmu untuk memulai game");
    }

    else
    {
        if(nama == "Jane" && peran == "")
        {
            console.log('Selamat datang di Dunia Werewolf, '+nama);
            console.log('Halo Penyihir '+nama+', kamu dapat melihat siapa yang menjadi werewolf!');
        }

        else
        {
            if(nama == "Jenita" && peran == "Guard")
            {
                console.log('Selamat datang di Dunia Werewolf, '+nama);
                console.log('Halo Guard '+nama+', kamu dapat melihat siapa yang menjadi werewolf!');
            }

            else
            {
                if(nama == "Junaedi" && peran == "Werewolf")
                {
                    console.log('Selamat datang di Dunia Werewolf, '+nama);
                    console.log('Halo Werewolf '+nama+', Kamu akan memakan mangsa setiap malam!');
                }
            }
        }
    }
}
// If-else

// Switch Case
var hari = 21; 
var bulan = 9; 
var tahun = 1945;

var bulanString = ""; 
switch(bulan) {
  case 1:   { bulanString ="Januari"; break; }
  case 2:   { bulanString ="Februari"; break; }
  case 3:   { bulanString ="Maret"; break; }
  case 4:   { bulanString ="April"; break; }

  case 5:   { bulanString ="Mei"; break; }
  case 6:   { bulanString ="Juni"; break; }
  case 7:   { bulanString ="Juli"; break; }
  case 8:   { bulanString ="Agustus"; break; }

  case 9:   { bulanString ="September"; break; }
  case 10:   { bulanString ="Oktober"; break; }
  case 11:   { bulanString ="November"; break; }
  case 12:   { bulanString ="Desember"; break; }
  default:  { console.log('Tidak terjadi apa-apa'); }}

var hasil = hari+" "+bulanString+" "+tahun;
console.log(hasil);
// Switch Case