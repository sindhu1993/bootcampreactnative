// No 1
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female",1970]];

var now = new Date()
var thisYear = now.getFullYear()

function arrayToObject(arr) {

    var ObjPeople = {};

    for(let x = 0; x < arr.length; x++) {
        
        if(arr[x][3] == NaN)
        {
            ObjPeople.age = "Age Kosong";
        }

        else
        {            
            if(parseInt(thisYear) - parseInt(arr[x][3]) > 0)
            {
                ObjPeople = {
                    firstName : arr[x][0],
                    lastName : arr[x][1],
                    gender : arr[x][2],
                    age : parseInt(thisYear) - parseInt(arr[x][3])
                }
                console.log((x+ 1) + ". " + arr[x][0] + ' ' + arr[x][1] + " : ",  ObjPeople);  
                // console.log(arr[x][3])  
            }
        }
    }
}

arrayToObject(people);
// No 1

// No 2
function shoppingTime(memberId='', money = 0) {

    var produk =[
    ['Sepatu brand Stacattu',1500000], 
    ['Baju brand Zoro', 500000],
    ['Baju brand H&N', 250000],
    ['Sweater brand Uniklooh', 175000],
    ['Casing Handphone', 50000],
    ];

    var ObjProduk = {};
    var ObjHasil = {};
    var moneyAwal = money;

    if(memberId == "" || money == 0)
    {
        console.log("Mohon maaf, toko X hanya berlaku untuk member saja")
    }
    else
    {
        if(money < 50000 )
        {
            console.log("Mohon maaf, uang tidak cukup")
        }

        else
        {
            var listPurchased = [];
            var arrProduk = [];
            var kembalian = 0;

            for (let index = 0; index < produk.length; index++) {
                ObjProduk = {
                    nama : produk[index][0],
                    harga : produk[index][1]
                }
                arrProduk.push(ObjProduk);
            }

            for(var k=0; k< arrProduk.length; k++){
                if(money >= arrProduk[k].harga){
                    listPurchased.push(arrProduk[k].nama);
                    money -= arrProduk[k].harga;
                    kembalian = money;
                    ObjHasil = {
                        memberId:memberId,
                        money:moneyAwal,
                        listPurchased:listPurchased,
                        changeMoney:kembalian
                    }
                }
            }

            console.log(ObjHasil);
        }
    }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
// No 2

// No 3
var input =[['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']];

function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    let harga = 2000;
    var arrAngkot = [];
    var objAngkot = {};

    if(arrPenumpang.length > 0)
    {
        for (let index = 0; index < arrPenumpang.length; index++) {
            var x = arrPenumpang[index][1];
            var y = arrPenumpang[index][2];
    
            if(rute.includes(x) && rute.includes(y)){
                var indexX = parseInt(rute.indexOf(x));
                var indexY = parseInt(rute.indexOf(y));
    
                var hargaTotal = harga * (indexY - indexX);

                if(hargaTotal < 0)
                {
                    console.log("Total Harga Minus");
                }

                objAngkot = {
                    penumpang : arrPenumpang[index][0],
                    nadikDari : arrPenumpang[index][1],
                    tujuan : arrPenumpang[index][2],
                    bayar : hargaTotal
                }
                arrAngkot.push(objAngkot);
            }
        }
        
    }
    else
    {
        console.log([]);
    }
    return arrAngkot;
  }

 console.log(naikAngkot(input));
// No 3