// No 1
function range(startNum, finishNum) 
{
    if(startNum == null || finishNum == null)
    {
        return -1;
    }
    else
    {
        if(startNum < finishNum)
        {
            // ascending
            var arrAscending = [];
            for(var i = parseInt(startNum); i<=parseInt(finishNum); i++)
            {
                arrAscending.push(i);
            }
            arrAscending.sort(function(startNum, finishNum){return startNum - finishNum });
            return arrAscending;
        }

        else
        {
            if(finishNum < startNum)
            {
                // descending
                var arrDescending = [];
                for(var i = parseInt(finishNum); i<=parseInt(startNum); i++)
                {
                    arrDescending.push(i);
                }
                arrDescending.sort(function(startNum, finishNum){return finishNum - startNum });
                return arrDescending;
            }
        }
    }
}

console.log(range(10,1));
// No 1

// No 2
function rangeWithStep(startNum, finishNum, step) 
{
    if(startNum == null || finishNum == null || step == null)
    {
        return -1;
    }
    else
    {
        if(startNum < finishNum)
        {
            // ascending
            var arrAscending = [];
            for(var i = parseInt(startNum); i<=parseInt(finishNum); i=i+parseInt(step))
            {
                arrAscending.push(i);
            }

            arrAscending.sort(function(startNum, finishNum){return startNum - finishNum });
            return arrAscending;
        }

        else
        {
            if(finishNum < startNum)
            {
                // descending
                var arrDescending = [];
                for(var i = parseInt(startNum); i>=parseInt(finishNum); i=i-parseInt(step))
                {
                    arrDescending.push(i);
                }
                // arrDescending.sort(function(startNum, finishNum){return finishNum - startNum });
                return arrDescending;
            }
        }
    }
}
console.log(rangeWithStep(29,2,4));
// No 2

// No 3
function sum(startNum, finishNum, step) 
{
    if(startNum == undefined && finishNum == undefined && step == undefined)
    {
        return 0;
    }

    if(startNum == 1 && finishNum == undefined && step == undefined)
    {
        return 1;
    }

    if(startNum !== undefined && finishNum !== undefined)
    {
        step = 1;
        if(startNum < finishNum)
        {
            // ascending
            var arrAscending = [];
            for(var i = parseInt(startNum); i<=parseInt(finishNum); i=i+parseInt(step))
            {
                arrAscending.push(i);
            }
            
            arrAscending.sort(function(startNum, finishNum){return startNum - finishNum });
            var sumAscending =  arrAscending.reduce(function(a,b){return a+b});
            return sumAscending;
        }

        else
        {
            if(finishNum < startNum)
            {
                // descending
                var arrDescending = [];
                for(var i = parseInt(startNum); i>=parseInt(finishNum); i=i-parseInt(step))
                {
                    arrDescending.push(i);
                }
                var sumDescending =  arrDescending.reduce(function(a,b){return a+b});
                return sumDescending;
            }
        }
    }  
}

console.log(sum(15,10));
// No 3

// No 4
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

function dataHandling(input)
{
    for(var i=0; i < input.length; i++){
        console.log("Nomor ID : "+input[i][0]+" \nNama Lengkap : "+input[i][1]+" \nTTL : "+input[i][2]+" "+input[i][3]+" \nHobi : "+input[i][4]+"\n");
    }
}

console.log(dataHandling(input));

// No 4

// No 5
function balikKata(words)
{
    var currentWords = words;
    var newWords = '';

    for(let i = words.length - 1; i >= 0; i--)
    {
        newWords = newWords + currentWords[i];
    }
    return newWords;
}

console.log(balikKata("Kasur Rusak"));
// No 5

// No 6
var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(input2)
{
    input2.splice(4,1);
    input2.splice(1,1,"Roman Alamsyah Elsharawy");
    input2.splice(2, 1, "Provinsi Bandar Lampung"); 
    input2.push("Pria","SMA Internasional Metro");
    
    var bulanString = ""; 
    var splitBulan = input2[3].split("/");
    var bulanPecah = splitBulan[1];

    switch(bulanPecah) {
    case '01':   { bulanString ="Januari"; break; }
    case '02':   { bulanString ="Februari"; break; }
    case '03':   { bulanString ="Maret"; break; }
    case '04':   { bulanString ="April"; break; }

    case '05':   { bulanString ="Mei"; break; }
    case '06':   { bulanString ="Juni"; break; }
    case '07':   { bulanString ="Juli"; break; }
    case '08':   { bulanString ="Agustus"; break; }

    case '09':   { bulanString ="September"; break; }
    case '10':   { bulanString ="Oktober"; break; }
    case '11':   { bulanString ="November"; break; }
    case '12':   { bulanString ="Desember"; break; }
    default:  { console.log('Bulan Tidak Ada'); }}

    // console.log(bulanString+"\n"+ splitBulan);
    
    var split = input2[3].split("/");

    arrTgl = [];
    split.sort(function (value1, value2) { return value2 - value1 });

    var stringNama = "";
    var sliceNama = "";
   
    console.log(input2);
    console.log(bulanString);
    console.log(split.toString());
    console.log(splitBulan.join("-"));
    stringNama = input2[1];
    
    if(stringNama.length > 15)
    {
        sliceNama = stringNama.slice(0,15);
    }

    console.log(sliceNama);
}

dataHandling2(input2);
// No 6