// No 1
function teriak()
{
    return "Halo Sanbers!";
}
console.log(teriak());
// No 1

// No 2
var num1 = 12
var num2 = 4
 
function kalikan(num1, num2)
{
    return num1 * num2;
}

var hasilKali = kalikan(num1, num2)
console.log(hasilKali);
// No 2

// No 3
var nama = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
function introduce(nama, age, address, hobby)
{
    if(parseInt(age) < 1)
    {
        console.log("Umur Minimal Angka 1");
    }

    else
    {
        if(age > 1)
        {
            return "Nama saya "+nama+", umur saya "+age+" tahun, alamat saya di "+address+", dan saya punya hobby yaitu "+hobby+"!";  
        }
        else if(age < 0)
        {
            return "Umur harus dibawah 1";
        }
        else
        {
            return "Inputan Umur Harus Angka";
        }
    } 
}

var perkenalan = introduce(nama, age, address, hobby)
console.log(perkenalan)
// No 3 