// Release 0
class Animal {
    constructor(name) {
        this.animalname = name;
        this.property_leg = 4;
        this.cold_blooded = false;
    }
    get name() {
        return this.animalname
    }
    get legs(){
        return this.property_leg
    }
    cold_blooded(){
        return this.cold_blooded
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name)
console.log(sheep.legs) 
console.log(sheep.cold_blooded)

// Release 1

class Ape extends Animal {
    yell(){
        return "Auooo"
    }
}
var sungokong = new Ape("Kera Sakti")
console.log(sungokong.yell())

class Frog extends Animal {
    constructor(name){
        super(name)
    }
    jump(){
        return "hop hop"
    }
}
var kodok = new Frog("buduk")
console.log(kodok.jump())


// No 2
class Clock {
    constructor(template){
        this.template = template;
        this.timer;
    }

    render=()=>{
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        // var h = this.template.toString().replace('h', hours);
        var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }

    stop=()=>{
        // var timer;
        clearInterval(this.timer);
    }

    start=()=>{
        // var timer;
        this.render();
        this.timer = setInterval(()=>this.render(), 1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  
// No 2
