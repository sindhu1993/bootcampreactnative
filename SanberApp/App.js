import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Telegram from './Tugas/Tugas12/Telegram.js';
import LoginScreen from './Tugas/Tugas13/LoginScreen.js';
import AboutScreen from './Tugas/Tugas13/AboutScreen.js';
import RestApi from './Tugas/Tugas14/RestApi.js';
import Login from './Tugas/Tugas15/Pages/Login.js';
import Tugas15 from './Tugas/Tugas15/index.js';
import Index from './Quiz3/index'
import LatihanFirebase from './Tugas/Latihan Authentikasi/index'

export default function App() {
  return (
    <LatihanFirebase/>
    // <Tugas15/>
    // <Index/>
  );
}

// var firebaseConfig = {
//   apiKey: "AIzaSyAYovVBfe6rDI1Roo3xfIIyphloP3uLQ0A",
//   authDomain: "authenticationfirebasern-99339.firebaseapp.com",
//   projectId: "authenticationfirebasern-99339",
//   storageBucket: "authenticationfirebasern-99339.appspot.com",
//   messagingSenderId: "1021288969240",
//   appId: "1:1021288969240:web:919352e5934d487348bf6d"
// };

// // Initialize Firebase
// firebase.initializeApp(firebaseConfig);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    color: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
