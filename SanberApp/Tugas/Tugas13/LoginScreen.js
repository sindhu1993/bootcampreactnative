import React, { Component } from 'react'
import { StyleSheet, View, Image,Text, TextInput, TouchableHighlight } from 'react-native'

export default class LoginScreen extends Component{
    
    render(){
        return (
            <View style={styles.container}>
                <View>
                <Image 
                    style={styles.logo}
                    source={require('./asset/logo.png')}
                />
                    <Text style={styles.txtLogin}>LOGIN</Text>
                    <Text style={styles.txtUsername}>Username</Text>
                    <TextInput style={styles.inputUsername} placeholder="Your Username..."></TextInput>
                    <Text style={styles.txtPassword}>Password</Text>
                    <TextInput style={styles.inputPassword} placeholder="Your Password..." autoCompleteType="password"></TextInput>
                        <View style={styles.divDaftar}>
                            <TouchableHighlight
                            style={styles.btnDaftar}
                            underlayColor='#fff'>
                                <Text style={styles.btnText}>Daftar</Text>
                            </TouchableHighlight>
                            <Text style={styles.txtAtau}>Atau</Text>
                            <TouchableHighlight
                            style={styles.btnMasuk}
                            underlayColor='#fff'>
                                <Text style={styles.btnTextMasuk}>Masuk</Text>
                            </TouchableHighlight>
                        </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        // flex:1,
        alignItems:'center',
        // flexDirection:'column',
        // grayBackground: "#EFEFEF"
        backgroundColor:'#FFF',
    },
    btnDaftar:{
        marginRight:40,
        marginLeft:40,
        marginTop:10,
        paddingTop:10,
        paddingBottom:10,
        backgroundColor:'#F05ACF',
        borderRadius:10,
        borderWidth: 1,
        borderColor: '#fff'
      },
      btnMasuk:{
        marginRight:40,
        marginLeft:40,
        marginTop:10,
        paddingTop:10,
        paddingBottom:10,
        backgroundColor:'#FAF4F9',
        borderRadius:10,
        borderWidth: 1,
        borderColor: '#FB21FF'
      },
    btnTextMasuk:{
        color:'#0500EF',
        textAlign:'center'
    },
    btnText:{
        color:'#fff',
        textAlign:'center',
    },
    divDaftar:{
        flex:1,
        paddingTop: 10,
        // flexDirection:'column'
    },
    inputPassword:{
        height:40,
        borderColor:'#A285DF',
        borderWidth:1,
        backgroundColor:'#F6F6F6'
    },  
    inputUsername:{
        height:40,
        borderColor:'#A285DF',
        borderWidth:1,
        backgroundColor:'#F6F6F6'
    },
    txtAtau:{
        marginTop:10,
        marginBottom:10,
        textAlign:'center',
        fontSize:14
    }, 
    txtLogin:{
        margin:20,
        textAlign:'center',
        fontSize:20,
        color:'#F05ACF'
    },
    txtUsername:{
        marginBottom:10,
        textAlign:'left',
        fontSize:14
    },
    txtPassword:{
        marginBottom:10,
        marginTop:10,
        textAlign:'left',
        fontSize:14
    },    
    logo:{
        width:300,
        height:100,
        // resizeMode:'center'
    }
});