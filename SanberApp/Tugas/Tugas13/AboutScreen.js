import React, { Component } from 'react'
import { StyleSheet, View, Image,Text, TextInput, TouchableHighlight } from 'react-native'

export default class AboutScreen extends Component{
    
    render(){
        return (
            <View style={styles.container}>
                <View>
                <Text style={styles.txtLogin}>Tentang Saya</Text>
                <Image 
                    style={styles.logo}
                    source={require('./asset/user1.png')}
                />
                    
                    <Text style={styles.txtUsername}>Albertus Kusuma</Text>
                    <Text style={styles.txtUsername}>React Native Developer</Text>
                    
                    <View style={styles.sectionPortofolio}>
                        <Text style={styles.txtPortofolio}>Portofolio</Text>
                        <View style={styles.divPortofolio}>
                            <Image
                                style={styles.github}
                                source={require('./asset/github.png')}
                            />
                            <Text style={styles.listText}>@Kusuma</Text>
                        </View>
                    </View>

                    <View>
                        <Text style={styles.txtPortofolio}>Akun Media</Text>
                        <View style={styles.divPortofolio}>
                            <Image
                                style={styles.facebook}
                                source={require('./asset/fb.png')}
                            />
                            <Text style={styles.listText}>@Kusuma</Text>
                            
                        </View>
                        <View style={styles.divPortofolio}>
                            <Image
                                style={styles.facebook}
                                source={require('./asset/ig.jpg')}
                            />
                            <Text style={styles.listText}>@Kusuma.ig</Text>
                            
                        </View>
                    </View>
                    
                   
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        // flex:1,
        height:'100%',
        alignItems:'center',
        // flexDirection:'column',
        // grayBackground: "#EFEFEF"
        backgroundColor:'#FFF',
    },
    divPortofolio:{
        backgroundColor:'#F8F8F8',
        flexDirection:'row',
        marginBottom:5
    },
    sectionPortofolio:{
        marginBottom:20,
    },
    listText:{
        marginTop:20,
        marginBottom:20,
        marginLeft:10,
        fontSize:14
    },
    txtPortofolio:{
        marginTop:10,
        marginBottom:10,
        textAlign:'left',
        fontSize:14
    }, 
    txtLogin:{
        margin:20,
        textAlign:'center',
        fontSize:20,
        color:'#F05ACF'
    },
    txtUsername:{
        marginBottom:5,
        textAlign:'left',
        fontSize:14,
        alignSelf:'center'
    },
    github:{
        width:50,
        height:50,
        alignSelf:'center'
        // resizeMode:'center'
    },
    facebook:{
        width:50,
        height:50,
        alignSelf:'center'
        // resizeMode:'center'
    },
    logo:{
        width:100,
        height:100,
        alignSelf:'center'
        // resizeMode:'center'
    }
});