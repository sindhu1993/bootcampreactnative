import React from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'
import firebase from 'firebase'

export default function Home({navigation}) {

    const firebaseConfig = {
        apiKey: "AIzaSyAYovVBfe6rDI1Roo3xfIIyphloP3uLQ0A",
        authDomain: "authenticationfirebasern-99339.firebaseapp.com",
        projectId: "authenticationfirebasern-99339",
        storageBucket: "authenticationfirebasern-99339.appspot.com",
        messagingSenderId: "1021288969240",
        appId: "1:1021288969240:web:919352e5934d487348bf6d"
    };

    if(!firebase.apps.length){
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
    }

    const logout=()=>{
        // console.log(email,password);
        firebase.auth().signOut()
        .then(()=>{
            console.log('logout berhasil')
            navigation.navigate("LoginScreen")
        }).catch(()=>{
            console.log('logout gagal')
        })
    }

    return (
        <View>
            <Text>Home</Text>
            <Button title="Logout" onPress={logout}/>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    }
})
