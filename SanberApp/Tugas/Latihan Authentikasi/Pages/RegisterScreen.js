import React,{useState} from 'react'
import { StyleSheet, Text, View, TextInput, Button } from 'react-native'
import firebase from 'firebase'

export default function RegisterScreen({navigation}) {

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const firebaseConfig = {
        apiKey: "AIzaSyAYovVBfe6rDI1Roo3xfIIyphloP3uLQ0A",
        authDomain: "authenticationfirebasern-99339.firebaseapp.com",
        projectId: "authenticationfirebasern-99339",
        storageBucket: "authenticationfirebasern-99339.appspot.com",
        messagingSenderId: "1021288969240",
        appId: "1:1021288969240:web:919352e5934d487348bf6d"
    };

    if(!firebase.apps.length){
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
    }

    const register=()=>{
        // console.log(email,password);
        const data = {
            email,
            password
        }
        console.log(data)
        firebase.auth().createUserWithEmailAndPassword(email,password)
        .then(()=>{
            console.log('Register berhasil')
            navigation.navigate("LoginScreen")
        }).catch(()=>{
            console.log('Register gagal')
        })
    }

    return (
        <View style={styles.container}>
        <Text>Login</Text>
        <TextInput placeholder="Email..." style={styles.input} value={email} onChangeText={(value)=> setEmail(value)}></TextInput>
        <TextInput placeholder="Password..." style={styles.input} value={password} onChangeText={(value)=> setPassword(value)}></TextInput>
        <Button title="Register" onPress={register}/>

        {/* <Text >Buat Akun</Text> */}

        {/* <Button onPress={()=>navigation.navigate('MyDrawer',{
            screen:'App',params:{
                screen:'AboutScreen'
            }
        })} 
            title="Menuju Halaman HomeScreen"
        /> */}
    </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    input:{borderWidth:1,borderRadius:25,marginBottom:12,paddingHorizontal:18},
})
