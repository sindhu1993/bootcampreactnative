import React, {useState,useEffect} from 'react'
import { Button, StyleSheet, Text, TextInput, View,Image, TouchableOpacity } from 'react-native'
import Axios from 'react-native-axios'

const Item=({title,value, onPress, onDelete})=>{
    return (
        <View style={styles.itemContainer}>
            <TouchableOpacity onPress={onPress}>
            <Image style={styles.avatar} source={require('./asset/user1.png')}/>
            </TouchableOpacity>
            <View style={styles.desc}>
                <Text style={styles.descTitle}>{title}</Text>
                <Text style={styles.descValue}>{value}</Text>
            </View>
            {/* <Text style={styles.edit}>Edit</Text> */}

            <TouchableOpacity onPress={onDelete}>
            <Text style={styles.delete}>Hapus</Text>
            </TouchableOpacity>
        </View>
    )
}

const RestApi=()=>{
    const [title, setTitle] = useState("")
    const [value, setValue] = useState("")
    const [news, setNews] = useState([])
    const [button, setButton] = useState("Simpan")
    const [selectedNews, setSelectedNews] = useState({})

    useEffect(() => {
        getData();
    }, [])

    const submit=()=>{
        const data={
            title,
            value
        }
        console.log('data : ',data)


        if(button === 'Simpan'){
            Axios.post(`https://achmadhilmy-sanbercode.my.id/api/v1/news`,data)
            .then(res => {
                console.log("res : ",res)
                setValue("")
                setTitle("")
                getData();
            })
        } else if(button === 'Update'){
            Axios.put(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${selectedNews.id}`,data)
            .then(res => {
                console.log("res : ",res)
                setValue("");
                setTitle("");
                getData();
                setButton("Simpan");
            })
        }
    }

    const getData=()=>{
        Axios.get(`https://achmadhilmy-sanbercode.my.id/api/v1/news`)
        .then(res => {
            console.log("res : ",res)
            setNews(res.data.data)
        })
    }

    const selectItem=(item)=>{
        console.log('item : ',item)
        setTitle(item.title);
        setValue(item.value);
        setButton("Update");
        setSelectedNews(item);
    }

    const deleteItem=(item)=>{
        Axios.delete(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${item.id}`)
        .then(res => {
            console.log("res : ",res)
            getData();
        })
    }

    return (
    <View style={styles.container}>
        <Text style={styles.textTitle}>CRUD Rest Api</Text>
        <TextInput placeholder="Input Title" style={styles.input} value={title} onChangeText={(value)=> setTitle(value)}></TextInput>
        <TextInput placeholder="Input Value" style={styles.input} value={value} onChangeText={(value)=> setValue(value)}></TextInput>
        <Button title={button} onPress={submit}/>
        <View style={styles.line}/>

        {news.map(n=>{
            return <Item key={n.id} title={n.title} value={n.value} 
            onPress={()=>selectItem(n)}
            onDelete={()=>deleteItem(n)}
            />
        })}
    </View>
    )
}

export default RestApi

const styles = StyleSheet.create({
    container:{padding:20},
    textTitle:{textAlign:'center',marginBottom:20},
    line:{height:2, backgroundColor:'black',marginVertical:10},
    input:{borderWidth:1,borderRadius:25,marginBottom:12,paddingHorizontal:18},
    avatar:{width:80, height:80, borderRadius:100},
    itemContainer:{flexDirection:'row',marginBottom:20},
    desc:{marginLeft:18,flex:1},
    descTitle:{fontSize:20,marginBottom:10},
    descValue:{fontSize:12},
    // edit:{fontSize:20,color:'orange',fontWeight:'bold',marginRight:5},
    delete:{fontSize:20,color:'red',fontWeight:'bold'}
});


