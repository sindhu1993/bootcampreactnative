import React, { useEffect } from 'react'
import { useState } from 'react'
import { StyleSheet, Text, View, Image, Button, FlatList, ScrollView } from 'react-native'
import { Data }from './data'


export default function Home({route, navigation}) {
    const { username } = route.params;
    // const { username } = "a";
    const [totalPrice, setTotalPrice] = useState(0);

    const currencyFormat=(num)=> {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
      };

    const updateHarga =(price)=>{
        console.log("UpdatPrice : " + price);
        const temp = Number(price) + totalPrice;
        console.log(temp)
        setTotalPrice(temp)
        
        //? #Bonus (10 poin) -- HomeScreen.js --
        //? agar harga dapat update misal di tambah lebih dari 1 item atau lebih -->
            
    }

    const _renderItem = ({ item }) => {
        return (
          <View
            style={{
              borderRadius:10,
              padding: 16,
              backgroundColor: 'lightgrey',
              width: 150,
              height: 210,
              marginHorizontal:10
            }}>
            <Text>{item.title}</Text>
            <Image
                style={{ width:100,height:100 }}
                source={item.image}
            />
            <Text style={{ fontSize:10 }}>{item.harga}</Text>
            <Text style={{ fontSize:10 }}>{item.type}</Text>
            <Button title="Beli" onPress={()=>updateHarga(item.harga)}/>
          </View>
        );
      };

    return (
        <View style={styles.container}>
            <View style={{flexDirection:'row', justifyContent:"space-between", padding: 16}}>
                <View>
                    <Text>Selamat Datang,</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}>{username}</Text>
                </View>
                <View>
                    <Text>Total Harga:</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}> {currencyFormat(totalPrice)}</Text>
                </View>
            </View>
            <View style={{alignItems:'center',  marginBottom: 20, paddingBottom: 60}}>
            {/* //? #Soal No 2 (15 poin) -- Home.js -- Function Home
            //? Buatlah 1 komponen FlatList dengan input berasal dari data.js   
            //? dan memiliki 2 kolom, sehingga menampilkan 2 item per baris (horizontal) -->

            //? #Soal No 3 (15 poin) -- HomeScreen.js --
             //? Buatlah styling komponen Flatlist, agar dapat tampil dengan baik di device untuk layouting bebas  --> */
             
             }

             <ScrollView horizontal>
                <FlatList
                // data={[{ key: 1 }, { key: 2 }, { key: 3 },{ key: 4 }]}
                data = {Data}
                renderItem={_renderItem}
                numColumns={2}
                        ItemSeparatorComponent={() => <View style={{margin: 2}}/>}
                />
                </ScrollView>

            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,        
        backgroundColor:'white', 
    },  
    content:{
        width: 150,
        height: 220,        
        margin: 5,
        borderWidth:1,
        alignItems:'center',
        borderRadius: 5,
        borderColor:'grey',    
    },
        
})